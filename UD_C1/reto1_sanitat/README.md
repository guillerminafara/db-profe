# Reto 1: Consultas básicas

FARA SANTEYANA, María Guillermina

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en:[LINK GITHUB](https://gitlab.com/guillerminafara/db-profe/-/tree/main/UD_C1/reto1_sanitat?ref_type=heads)

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD,NOM,TELEFON FROM HOSPITAL;
```


## Query 2
Muestre los hospitales existentes (número, nombre y teléfono) que tengan una letra A en la segunda posición del nombre.
Seleccinamos las 3 columnas solicitadas (HOSPITAL_COD, NOM y TELEFON ), de la tabla Hospital. Para cumplir la condición de la letra tomamos NOM y comparamos con el string `_a%` que referiere a una palabra con primera casilla sin determinar, segunda que sea una 'a' y pudiendo finalizar indistintamente
```sql
SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL 
WHERE NOM LIKE '_a%';
```
alternativa
```sql
SELECT HOSPITAL_COD, NOM, TELEFON
FROM HOSPITAL 
WHERE substring(NOM,2,1)= 'a';
```
## Query 3 
Muestre los trabajadores(código hospital, código sala, número empleado y apellido) existentes.
Seleccionaremos los atributos solicitados (`HOSPITAL_COD` , `SALA_COD`, `EMPLEAT_NO`, `COGNOM`) de la tabla Plantilla.
```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM
FROM PLANTILLA;
```

## Query 4
 Muestre los trabajadores (código hospital, código sala, número empleado y apellido) que no sean del turno de noche.
 Seleccionamos los atributos solicitados de la tabla plantilla filtrándolos por la condición que el turno sea distintos de 'N'.  
```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM, TORN
FROM PLANTILLA
WHERE TORN != 'N';
```
ALTERNATIVA 
```sql
SELECT HOSPITAL_COD, SALA_COD, EMPLEAT_NO, COGNOM, TORN
FROM PLANTILLA
WHERE TORN <> 'N';
```
## Query 5
Muestre a los enfermos nacidos en 1960.
Seleccionaremos todas las columnas de la tabla `MALALT`, es decir enfermos que cumplan la condición `nacidos en 1960`, para eso, utilizamos la función `year` la cual nos solicita una fecha como parámetro, esta extrae el registro y corrobora que sea igual al año 1960.
```sql
SELECT *
FROM MALALT
WHERE year(DATA_NAIX)=1960;
```

## Query 6
Muestre a los enfermos nacidos a partir del año 1960.
Seleccionaremos todas las columnas de la tabla `MALALT`, utilizamos la función year que extrae los valores de tipo fecha y comparamos que estos sean mayores al año 1960 
```sql
SELECT *
FROM MALALT
WHERE year(DATA_NAIX)>=1960;
```
alternativa
```sql
SELECT *
FROM MALALT
WHERE year(DATA_NAIX)>=DATE('1960');
```
