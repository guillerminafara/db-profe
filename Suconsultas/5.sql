-- Álbumes que tienen más de 15 canciones, junto a su artista.

SELECT  AlbumId
       ,Artist.Name
       ,ArtistId
FROM Album
JOIN Artist USING
(ArtistId
)
WHERE AlbumId in((
            SELECT  AlbumId
            FROM Track
            GROUP BY  AlbumId
            HAVING COUNT( TrackId) > 15))
            ORDER BY Album.AlbumId ASC;

-- SELECT * 
-- FROM Album 
-- HAVING AlbumId IN((SELECT AlbumId
-- FROM Track 
-- GROUP BY AlbumId
-- HAVING count( TrackId) >15));

-- select count(Track.TrackId) as "cantidad", Album.Title
-- from Album join Track using(AlbumId)
-- group by Album.AlbumId 
-- having count(Track.TrackId) >15 
-- order by `cantidad` asc;


