-- Ventas totales de cada empleado
SELECT  Employee.EmployeeId
       ,Employee.FirstName
       ,totall
FROM Employee
LEFT JOIN
(
	SELECT  InvoiceId
	       ,Customer.SupportRepId
	       ,Customer.CustomerId
	       ,SUM(Total)as totall
	FROM Invoice
	JOIN Customer USING
	(CustomerId
	)
	GROUP BY  Customer.SupportRepId
) AS tabla on(Employee.EmployeeId = tabla.SupportRepId)