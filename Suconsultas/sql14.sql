-- Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes
-- más antiguos tienen un ID más bajo.
SELECT  Artist.ArtistId
       ,Artist.Name
       ,tabla.ultimo
FROM Artist
JOIN
(
	SELECT  ArtistId
	       ,Title
	       ,MAX(AlbumId) AS ultimo
	FROM Album
	GROUP BY  ArtistId
) AS tabla USING (ArtistId)

-- consulta piola
-- SELECT Album.AlbumId, Artist.Name, Album.Title 
-- from Artist join Album USING(ArtistId)
-- GROUP by (ArtistId)
