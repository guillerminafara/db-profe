--Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.
SELECT  CustomerId
       ,Customer.FirstName
       ,totall
FROM Customer
JOIN
(
	SELECT  CustomerId
	       ,SUM(Total) AS totall
	FROM Invoice
	GROUP BY  CustomerId
	ORDER BY  Invoice.CustomerId ASC
) AS tabla USING(CustomerId)

-- version piola
-- SELECT CustomerId, SUM(Total) from Invoice  
-- GROUP by CustomerId
-- ORDER BY `Invoice`.`CustomerId` ASC