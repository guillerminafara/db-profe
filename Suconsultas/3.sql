-- Mostrar las listas de reproducción en las que hay canciones de reggae.
SELECT distinct
    PlaylistTrack.PlaylistId,
    Playlist.Name
FROM PlaylistTrack
    JOIN Playlist USING (PlaylistId)
WHERE
   PlaylistTrack.TrackId IN (
        SELECT TrackId
        FROM Track
        WHERE
            GenreId LIKE(
                SELECT GenreId
                FROM Genre
                WHERE
                    Genre.Name LIKE "Reggae"
            )
    );