FARA SANTEYANA, María Guillermina

## Introducción 
Hemos trabajado con la tabla Chinook para adentrarnos en las subconsultas. Acceso al repositorio [LINK GITHUB](https://gitlab.com/guillerminafara/db-profe/-/tree/main/Suconsultas?ref_type=heads)
### Query 1
**Obtener las canciones con una duración superior a la media.**
Para esta query haremos dos consultas separadas y luego las uniremos. 
Haremos una consulta que tome algunos datos de la tabla Track, dándole la condición de que la cantidad de los milisegundos de cada Track sea mayor al promedio general. Para esto úlltimoo obtendremos con ``AVG()``el promedio de duracion de las caciones por milisegundos y ordenaremos de menor a mayor duración. 
```sql
SELECT  TrackId
       ,Name
       ,Milliseconds
FROM Track
WHERE Milliseconds > (
            SELECT  AVG(Milliseconds)
            FROM Track )
            ORDER BY Milliseconds ASC;
```

### Query 2
**Listar las facturas del cliente cuyo email es “emma_jones@hotmail.com”.**
Trabajando con tabla Customer buscaremos el id del cliente que tenga registrado el mail en cuestion (esta seria nuestra subconsulta). Con este id podemos realizar una consulta principal con la tabla Factura(Invoice) buscando aquellas que coincidan en ``CustomerId``. Finalmente ordenaremos el resultado de màs a menos reciente.  
```sql
SELECT *
FROM Invoice
WHERE
    CustomerId like(
        SELECT CustomerId
        FROM Customer
        WHERE
            email LIKE "emma_jones@hotmail.com"
    )
ORDER BY InvoiceDate DESC;
```
### Query 3
**Mostrar las listas de reproducción en las que hay canciones de reggae.**
Para esta query usaremos 4 tablas Genre, Playlist, PlaylistTrack y Track. Comenzaremos con la subconsulta con la que conseguiremos el id del Género "reggae", este id es el que usaremos para buscar los id de las canciones (TrackId) que coincidan en este GenreId. Los Identificadores de las canciones que obtuvimos los necesitamos para buscar las playlist en las que se encuentran estas canciones usaremos, ya que TrackId es común en tablas PlaylisTrack y Track un IN para ello. Finalmente haremos un Join para conseguir el resto de datos de la tabla Playlist como ser su nombre, o el identificador de la Playlist
```sql
SELECT distinct
    PlaylistTrack.PlaylistId,
    Playlist.Name
FROM PlaylistTrack
    JOIN Playlist USING (PlaylistId)
WHERE
   PlaylistTrack.TrackId IN (
        SELECT TrackId
        FROM Track
        WHERE
            GenreId LIKE(
                SELECT GenreId
                FROM Genre
                WHERE
                    Genre.Name LIKE "Reggae"
            )
    );
```

### Query 4
**Obtener la información de los clientes que han realizado compras superiores a 20€.**
Buscaremos los id. de los clientes que hicieron compras superiores a 20 euros en la tabla Invoice, las linesas obtenidas las usaremos para buscar coincidencia de estos id. en la Tabla Customer y obtener el el resto de datos.
```sql
SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName
FROM Customer
WHERE
    Customer.CustomerId IN (
        SELECT Invoice.CustomerId
        FROM Invoice
        WHERE
            Total > 20
    );
```

### Query 5
**Álbumes que tienen más de 15 canciones, junto a su artista.**
```sql
SELECT  AlbumId
       ,Artist.Name
       ,ArtistId
FROM Album
JOIN Artist USING
(ArtistId
)
WHERE AlbumId in((
            SELECT  AlbumId
            FROM Track
            GROUP BY  AlbumId
            HAVING COUNT( TrackId) > 15))
            ORDER BY Album.AlbumId ASC;
```

### Query 6
**Obtener los álbumes con un número de canciones superiores a la media.**
```sql
SELECT  AlbumId
       ,Album.Title
       ,COUNT(TrackId) AS NUM
FROM Track
JOIN Album USING
(AlbumId
)
GROUP BY  AlbumId
HAVING NUM > (
SELECT  AVG(NUM) AS avg
FROM
(
	SELECT  COUNT(TrackId) AS NUM
	FROM Track
	GROUP BY  AlbumId
) AS subconsulta );
```

### Query 7
**Obtener los álbumes con una duración total superior a la media.**
```sql
SELECT  Album.AlbumId
       ,Album.Title
       ,SUM(Milliseconds) AS sumaaa
FROM Track
JOIN Album USING
(AlbumId
)
GROUP BY  AlbumId
HAVING sumaaa > (
        SELECT  AVG(suma)
        FROM
(
            SELECT  AlbumId
                ,SUM(Milliseconds) AS suma
            FROM Track
            GROUP BY  AlbumId
) AS tabla);
```
### Query 8
**Canciones del género con más canciones**
```sql
SELECT  GenreId
       ,Genre.Name
       ,MAX(NumeroCanciones)
FROM Genre
JOIN
(
	SELECT  GenreId
	       ,COUNT(*) AS NumeroCanciones
	FROM Track
	GROUP BY  GenreId
) AS subconsulta USING(GenreId);
```

### Query 9
**Canciones de la playlist con más canciones.**
```sql
SELECT  PlaylistId
       ,Track.TrackId
       ,Track.Name
FROM Track
JOIN
(
	SELECT  PlaylistId
	       ,TrackId
	FROM
	(
		SELECT  PlaylistId
		       ,TrackId
		       ,COUNT(TrackId)as numeroCanciones
		FROM PlaylistTrack
		GROUP BY  PlaylistId
	) AS subquery
	HAVING MAX(numeroCanciones)
) AS tabla;
```
### Query 10
**Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.**
```sql
SELECT  CustomerId
       ,Customer.FirstName
       ,totall
FROM Customer
JOIN
(
	SELECT  CustomerId
	       ,SUM(Total) AS totall
	FROM Invoice
	GROUP BY  CustomerId
	ORDER BY  Invoice.CustomerId ASC
) AS tabla USING(CustomerId);
```

### Query 11
**Obtener empleados y el número de clientes a los que sirve cada uno de ellos.**
```sql

SELECT  EmployeeId
       ,LastName
       ,(
SELECT  COUNT(CustomerId)
FROM Customer
WHERE customer.`SupportRepId` = employee.`EmployeeId` ) AS CantidadCustomer
FROM Employee;
```
### Query 12
**Ventas totales de cada empleado**
```sql
SELECT  Employee.EmployeeId
       ,Employee.FirstName
       ,totall
FROM Employee
LEFT JOIN
(
	SELECT  InvoiceId
	       ,Customer.SupportRepId
	       ,Customer.CustomerId
	       ,SUM(Total)as totall
	FROM Invoice
	JOIN Customer USING
	(CustomerId
	)
	GROUP BY  Customer.SupportRepId
) AS tabla on(Employee.EmployeeId = tabla.SupportRepId);
```
### Query 13 
**Álbumes junto al número de canciones en cada uno.**
```sql
SELECT AlbumId,
    tabla.Title,
    count(TrackId) as numCanciones
from Track
    JOIN (
        SELECT Album.AlbumId,
            Album.Title
        from Album
    ) as tabla USING(AlbumId)
GROUP by AlbumId;

```
### Query 14 
**Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.**
```sql
SELECT  Artist.ArtistId
       ,Artist.Name
       ,tabla.ultimo
FROM Artist
JOIN
(
	SELECT  ArtistId
	       ,Title
	       ,MAX(AlbumId) AS ultimo
	FROM Album
	GROUP BY  ArtistId
) AS tabla USING (ArtistId);
```
