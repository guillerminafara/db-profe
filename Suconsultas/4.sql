-- Obtener la información de los clientes que han realizado compras superiores a
-- 20€.

SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName
FROM Customer
WHERE
    Customer.CustomerId IN (
        SELECT Invoice.CustomerId
        FROM Invoice
        WHERE
            Total > 20
    );