--Canciones de la playlist con más canciones.
SELECT  PlaylistId
       ,Track.TrackId
       ,Track.Name
FROM Track
JOIN
(
	SELECT  PlaylistId
	       ,TrackId
	FROM
	(
		SELECT  PlaylistId
		       ,TrackId
		       ,COUNT(TrackId)as numeroCanciones
		FROM PlaylistTrack
		GROUP BY  PlaylistId
	) AS subquery
	HAVING MAX(numeroCanciones)
) AS tabla;