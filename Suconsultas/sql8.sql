
-- Canciones del género con más canciones 
SELECT  GenreId
       ,Genre.Name
       ,MAX(NumeroCanciones)
FROM Genre
JOIN
(
	SELECT  GenreId
	       ,COUNT(*) AS NumeroCanciones
	FROM Track
	GROUP BY  GenreId
) AS subconsulta USING(GenreId);