--Obtener las canciones con una duración superior a la media.
SELECT  TrackId
       ,Name
       ,Milliseconds
FROM Track
WHERE Milliseconds > (
            SELECT  AVG(Milliseconds)
            FROM Track )
            ORDER BY Milliseconds ASC