-- Obtener los álbumes con una duración total superior a la media.
SELECT  Album.AlbumId
       ,Album.Title
       ,SUM(Milliseconds) AS sumaaa
FROM Track
JOIN Album USING
(AlbumId
)
GROUP BY  AlbumId
HAVING sumaaa > (
SELECT  AVG(suma)
FROM
(
	SELECT  AlbumId
	       ,SUM(Milliseconds) AS suma
	FROM Track
	GROUP BY  AlbumId
) AS tabla)