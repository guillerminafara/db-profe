--Álbumes junto al número de canciones en cada uno.

SELECT AlbumId,
    tabla.Title,
    count(TrackId) as numCanciones
from Track
    JOIN (
        SELECT Album.AlbumId,
            Album.Title
        from Album
    ) as tabla USING(AlbumId)
GROUP by AlbumId;
