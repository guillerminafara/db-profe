-- Obtener los álbumes con un número de canciones superiores a la media.
SELECT  AlbumId
       ,Album.Title
       ,COUNT(TrackId) AS NUM
FROM Track
JOIN Album USING
(AlbumId
)
GROUP BY  AlbumId
HAVING NUM > (
SELECT  AVG(NUM) AS avg
FROM
(
	SELECT  COUNT(TrackId) AS NUM
	FROM Track
	GROUP BY  AlbumId
) AS subconsulta );