--Listar las facturas del cliente cuyo email es “emma_jones@hotmail.com”.

SELECT *
FROM Invoice
WHERE
    CustomerId like(
        SELECT CustomerId
        FROM Customer
        WHERE
            email LIKE "emma_jones@hotmail.com"
    )
ORDER BY InvoiceDate DESC;

-- select *
-- from Customer join Invoice on (Customer.CustomerId = Invoice.CustomerId)
-- where email like "emma_jones@hotmail.com" order by InvoiceId desc limit 5;