FARA SANTEYANA, María Guillermina
DAM1
    En el siguiente trabajo realizaremos algunas búsquedas básicas comenzando con la base de datos `empresa` y siguiendo con `videoclub`. En las últimas 4 consultas volveremos sobre los datos encontrados en `empresa`.

 El link hacia el repositorio podremos encontrarlo en el siguiente: [enlace](https://gitlab.com/guillerminafara/db-profe/-/tree/main/VideoClubEmpresa?ref_type=heads).


# Empresa

## Query 1 
La siguiente consulta selecciona el código de un producto(PROD_NUM) y su descripción(DESCRIPCIO) de la tabla llamada producte.
```sql
SELECT PROD_NUM, DESCRIPCIO
FROM producte;
```

## Query 2
La siguiente consulta selecciona el código de un producto(PROD_NUM) y su descripción(DESCRIPCIO) de la tabla llamada `producte`, pero filtrando para que los resultados contengan la palabra `tennis` en alguna parte de la de decripción.
```sql
SELECT PROD_NUM, DESCRIPCIO
FROM producte
WHERE DESCRIPCIO like "%tennis%";
```
## Query 3 
En esta consulta se seleccionará el código de cliente, el área en la que vive y el telefono de la tabla llamada `client`.
```sql 
SELECT CLIENT_COD, NOM, AREA, TELEFON
from client;
```
## Query 4
De la tabla cliente se recogeran el código de cliente, su nombre, y la ciudad donde viven los clientes que se encuentra en el area distinta de 636.
```sql
SELECT CLIENT_COD, NOM, CIUTAT
from client
WHERE AREA NOT LIKE 636;
```
## Query 5

De la tabla pedido(`comanda`) se seleccionarán el número de pedido, fecha en la que se realiza el pedido y la fecha de entrega.
```sql
SELECT COM_NUM,COM_DATA, DATA_TRAMESA
FROM comanda;

```
# Videoclub
## Query 6
Realizaremos una busqueda de los nombres y el telefono de los clientes que se encuentran en la table `client`.
```sql
SELECT Nom, Telefon
FROM client;
```
## Query7
Tomaremos el importe y la fecha de la tabla `factura`.
```sql
SELECT Import, Data
FROM factura;
```
## Query 8
Seleccionaremos de la tabla `detallfactura` la descripción el y el codigo de las factura que sea igual a 3. 
```sql
SELECT Descripcio, CodiFactura
FROM detallfactura
WHERE CodiFactura=3 ;
```

## Query 9
Seleccionaremos toda la información de las facturas y los resultados serán ordenados por el importe de estas del más grande hasta el más pequeño.
```sql
SELECT * 
FROM factura 
ORDER BY Import DESC;
```
## Query 10
Seleccionaremos la fila de la tabla actor pero filtrándolo por aquellos que el nombre comience por `X`.
```sql
SELECT * 
FROM actor
WHERE Nom LIKE "X%";
```
## Query 11
Agregamos a la tabla ACTOR 2 filas, en este caso `ximo` y `xavi` y le asignamos los códigos 4 y 5 correspondientemente. 
```sql
INSERT INTO actor 
values(4,"ximo");
```
```sql
INSERT INTO actor 
values(5,"xavi");
```


## Query 12 
Eliminaremos de la tabla actor aquellas filas donde el código de actor sean 4 y 5 en un solo comando. 
```sql
delete from actor 
where CodiActor=4 or CodiActor=5 ;
``` 


## Query 13 
Agregamos en este caso a Xordi y le asignamos el código 6. Lo particular de este comando es que probamos cambiando el orden de la información de la columna. 
```sql
INSERT INTO actor (Nom, CodiActor)
VALUES ("Xordi",6);
```
Agregamos de nuevo a `xavi`.
```sql
INSERT INTO actor 
values(5,"xavi");
```
## Query 14
Eliminamos en un solo comando, de la tabla de `actor`, a la s filas que correspondan con el código 6 y 5.
```sql
delete from actor where CodiActor in (6,5);
```
## Query 15
Actualizaremos por `Jordi` el nombre del actor `Xordi`. En las pc 
```sql
update actor
set Nom= "Jordi"
where Nom="Xordi";
```
```sql
update actor
set Nom= "Jordi"
where CodiActor = 6;
```
**A partir de acá, volvemos a trabaja con la base de datos empresa**
## Query 16
Haremos una búsqueda de los trabajadores dados de alta separando por año sin que se repitan los años y las filas 

```sql
select distinct YEAR(DATA_ALTA) from emp;
```
## Query 17

En primer lugar relizamos una consulta seleccionando las categorias de empleados de la tabla emp para saber qué catergorias existen. 
``` sql
SELECT OFICI
FROM emp;
```
Luego, realizamos la consulta para saber cuántas categorías existen
```sql
SELECT COUNT(distinct OFICI) 
FROM emp;
```
## Query 18
Buscaremos en la tabla `emp` a los empleados cuyas comisiones se presenten como no nulas.
```sql
select *
from emp
WHERE COMISSIO IS NOT NULL;
```
## Query 19
19. 2 QUE SE LLEVAN MAS COMISION 
Obtendremos toda la información de los empleados, dónde la comisión no sea nulla y los ordenaremos por el importe de esa comisión y ordenados de mayor a menor.
```sql
select *
from emp
WHERE COMISSIO IS NOT NULL
order by COMISSIO DESC;
```

[def]: https://gitlab.com/guillerminafara/db-profe/-/tree/main/VideoClubEmpresa?ref_type=heads