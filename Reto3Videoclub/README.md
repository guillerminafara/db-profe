
FARA SATEYANA, María Guillermina
En la siguiente presentación comenzamos a trabajar con consultas relacionando diferentes tablas, para ello hemos utilizado los ``JOINS``. [LINK GITLAB](https://gitlab.com/guillerminafara/db-profe/-/tree/main/Reto3Videoclub?ref_type=heads)

## Query 1
Seleccionamos el título de la tabla ``PELICULA`` y el nombre de su género de la tabla ``GENERE`` uniendo ambas tablas por su punto en común ``CodiGenere`` .

```sql
select Titol, GENERE.Descripcio 
from PELICULA inner join GENERE 
on (PELICULA.CodiGenere = GENERE.CodiGenere);
```

## Query 2

Seleccionamos toda la información de las tablas ``FACTURA`` y``CLIENT `` con la condición de que el nombre del cliente contenga la línea de carácteres ``María``. En este caso usamos el DNI como clave para la combinación de datos.
```sql
select * 
FROM FACTURA inner join CLIENT 
on (FACTURA.DNI = CLIENT.DNI) 
WHERE CLIENT.Nom like "%Maria%"; 
```
## Query 3
Buscaremos en las tablas pelicula y actor el nombre de este y el título de las peliculas donde haya una coincidencia en sus ``CodiActor`` , es decir, dónde este actor aparezca en la película siendo actor principal.
```sql
select Nom, Titol 
from PELICULA INNER JOIN ACTOR 
ON (PELICULA.CodiActor = ACTOR.CodiActor); 
```
## Query 4
Obtendremos los registros de película y de los actores que han participado en ellas relacionando la tabla ``PELICULA`` con ``INTERPRETADA`` en primer lugar y luego ``INTERPRETADA`` con ``ACTOR``. 
```sql
select PELICULA.*, INTERPRETADA.* 
from PELICULA JOIN INTERPRETADA  
on(PELICULA.CodiPeli = INTERPRETADA.CodiPeli) 
JOIN ACTOR 
ON (INTERPRETADA.CodiActor = ACTOR.CodiActor); 
```