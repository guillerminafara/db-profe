use Chinook;
select count(Track.TrackId), Album.Title
from Album join Track on (Album.AlbumId = Track.AlbumId)
group by Title order by count(Track.TrackId) desc;