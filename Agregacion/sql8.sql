SELECT A.FirstName as Empleado, S.FirstName as Supervisor, A.BirthDate
FROM Employee as A JOIN Employee as S on (A.ReportsTo = S.EmployeeId)
order by A.BirthDate desc limit 3;