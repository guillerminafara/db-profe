-- Query 20
-- Muestra los países en los que tenemos al menos 5 clientes.

SELECT Customer.Country, COUNT(DISTINCT Customer.CustomerId) AS "Número de clientes"
FROM Customer 
GROUP BY Customer.Country
HAVING COUNT(DISTINCT Customer.CustomerId) >= 5
ORDER BY "Número de clientes" DESC;