
use Chinook;
-- Muestra los álbumes ordenados por el número de canciones que tiene cada uno.
select Genre.GenreId, Genre.Name, count(*)
from Track join InvoiceLine
on (Track.TrackId = InvoiceLine.TrackId)
join Genre on (Track.GenreId=Genre.GenreId)
group by Genre.Name, Genre.GenreId order by count( *) desc;


