-- Muestra el importe medio, mínimo y máximo de cada factura.
SELECT min(total), max(total), avg(total) 
FROM invoice;