use Chinook;
-- Query 19
-- Lista los 6 álbumes que acumulan más compras.

select Album.AlbumId, Album.Title, Track.Composer, count(*)
from Album join Track on(Album.AlbumId = Track.AlbumId)
join InvoiceLine on (Track.TrackId = InvoiceLine.TrackId)
group by Album.AlbumId, Album.Title, Track.Composer order by count(*) desc limit 6;

select Album.AlbumId, Album.Title, Artist.Name, count(*)
from Album join Track on(Album.AlbumId = Track.AlbumId)
join InvoiceLine on (Track.TrackId = InvoiceLine.TrackId) 
join Artist on (Artist.ArtistId= Album.ArtistId)
group by Album.AlbumId, Album.Title, Artist.Name order by count(*) desc limit 6;