alumno: FARA SANTEYANA, María Guillermina
## Introducción 
En este trabajo nos centraremos en la estructura de la base de datos ``Reservas`` y las sentencias necesarias para crearla.
El código fuente correspondiente a este reto puede consultarse en:[LINK GITHUB](https://gitlab.com/guillerminafara/db-profe/-/tree/main/DDL?ref_type=heads)


## Crear Base de datos
Con el siguiente comando estaremos creando una base de datos llamada ``Reservas`` y dando la instrucción de trabajar sobre ella.
```sql
CREATE DATABASE Reservas;
USE Reservas;
```



## Crear tablas
### Tabla Pasajeros
Con el siguiente comando creamos la tabla ``Pasajeros`` con las columnas:
``id_pasajero``: como Clave Primaria de tipo numérico, por tal motivo no puede ser nula, ni estar repetida ya que se trata del número con el que asociaremos personas en nuestra base. Además en este caso he indicado que sea autoincremental, es decir el valor que toma es automático y va en incremento teniendo como referencia filas anteriores(tanto las creadas como los intentos). 
``pasaporte``: en sus filas puede incluir tanto letras, números como caracteres especiales, teniendo como extensión máxima 9 caracteres (largo que suelen tener los pasaportes), los filtrados para la validación del pasaporte lo haremos desde código java. Teniendo en cuenta el contexto de los registros de la base de datos esta información no puede ser nula ya que el pasajero debe tener asociado un número de identificación. A su vez, indicamos que no puede repetirse el número (UNIQUE) ya que, en nuestra base, no se admiten 2 pasajeros con el mismo dni en un mismo vuelo. 
``nombre``: admite como máximo 30 caracteres, espacio suficiente para el nombre del viajero. Tampoco puede ser nula esta información 


```sql
CREATE TABLE `Reservas`.`Pasajeros` (
  `id_pasajero` INT NOT NULL UNIQUE AUTO_INCREMENT,
  `pasaporte` VARCHAR(9) NOT NULL UNIQUE,
  `nombre` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id_pasajero`)
);
```
### Tabla Vuelos 
Con el siguiente comando estaremos creando la tabla ``Vuelos`` que tiene las columnas: 
``id_vuelo``: acepta hasta 6 caracteres, es señalada como clave primaria, es decir, es la clave con la que identificaremos cada fila creada y cada vuelo, por lo que no puede ser nula y debe ser única para evitar confusiones futuras. 
``origen``: la columna acepta un máximo de 11 caracteres. Refiere al punto de partida del vuelo por lo que este campo debe estar completo (not null) y debe ser válido(esta validación la veremos dentro del código en java).
``destino``: al igual que destino solo acepta 11 caracteres de extensión. Refiere al punto de llegada del vuelo por lo que debe estar completo (not null) y debe ser válido(esta validación la veremos dentro del código en java).
``fecha``: columna de tipo fecha. Refiere al día que parte el vuelo por ende, no puede ser un campo vacío (not null),``date`` restringe al formato norteamericano (yyyy-MM-dd) la información recibida.
``capacidad``: refiere al número de butacas que tendrá el vuelo, por lo que no puede ser una columna vacía. Solo admite números de como máximo 3 dígitos ya que refiere a un número en si y no un código.
```sql
CREATE TABLE `Reservas`.`Vuelos` (
  `id_vuelo` VARCHAR(6) NOT NULL,
  `origen` VARCHAR(11) NOT NULL,
  `destino` VARCHAR(11) NOT NULL,
  `fecha` DATE NOT NULL,
  `capacidad` INT(3) NOT NULL,
  PRIMARY KEY (`id_vuelo`)
);
```
### Tabla Vuelos_Pasajeros
Creamos la tabla ``Vuelos_Pasajeros`` como tabla de unión entre las tablas``Vuelos`` y ``Pasajeros`` por lo que en su composición nos encontraremos con las claves foráneas de éstas.
Encontramos las siguientes columnas:
``id_reserva``: será nuestra clave primaria para referirnos a las reservas que se han generado. Por ende, no puede ser nula ni estar repetida. También indicamos que sea autoincremental. Este dato decidimos sumarlo ya que permite el manejo de la información contenida en sus filas.
``id_vuelo``: no puede estar vacía y tiene una extensión de 6 caracteres como en su tabla (``vuelo``). Es una de las claves foráneas en esta tabla, por lo que no puede ser nula y su código debe existir en la tabla vuelo.
`` id_pasajero``: es la última clave foránea para relacionar con la tabla de pasajeros, tampoco puede estar vacío este dato y debe ser completado con un número existente en la tabla pasajeros.
`` num_asiento``: esta columna representa al número de asiento que el pasajero tendra en el vuelo por ente tiene que contar con un dato que validaremos con java y no puede ser null. 
```sql
CREATE TABLE `Vuelos_Pasajeros` (
  `id_reserva` int NOT NULL AUTO_INCREMENT,
  `id_vuelo` varchar(6) NOT NULL,
  `id_pasajero` int NOT NULL,
  `num_asiento` int NOT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `Vuelos_Pasajeros_ibfk_1` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`),
  CONSTRAINT `Vuelos_Pasajeros_ibfk_2` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`)
);
```
### Consultas sobre tablas 
La siguiente query nos muestra únicamente el nombre las tablas dentro de una base de datos seleccionada. En nuestro caso Reservas.

```sql
show tables;
```

Para complementar las consultas anteriores la sentencia ```DESCRIBE`` nos brinda mayor informacion sobre una tabla, mostrándonos sus columnas el tipo de datos que admite y sus restrcciones. 
```sql
describe pasajeros;
```
Una alternativa a la query anterior. Brinda la misma información.
```sql
show columns from pasajeros;
```
Las claves  primarias son ``id_vuelo``en tabla vuelo, ``id_pasajero `` en tabla pasajero e ``id_reserva`` en tabla Vuelos_Pasajeros. Las foráneas son ``id_vuelo`` e ``id_pasajero`` en tabla Vuelos_Pasajeros.

 Para poder generar una unión entre 2 tablas existententes crearemos la tercera tabla que guardará los id de las tablas a relacionar más algunos datos propios. Esta tercera tabla nos ayuda a manejar las relaciones y evita que los datos se repitan a la hora de hacer búsquedas. Un beneficio de esta relación es que la fila de cualquiera de las tablas principales, no podra ser eliminada si es que su id existe en un registro en la tabla de unión, es beneficioso ya que no permite eliminar datos de forma errónea dándole consistencia a los datos y evitarnos posibles problemas. Por ej. no podemos eliminar al Pasajero "Elsa Pato" si es que ha realizado la reserva de un vuelo. Para eso, primero deberemos eliminar la fila de la tabla de unión que tiene el id de Elsa, es decir primero debemos eliminar su reserva y posteriormente podremos eliminar al pasajero. Lo mismo ocurre con los Vuelos, para eliminar un vuelo primero deberemos eliminar todas las filas de la tabla Vuelos_Pasajeros(reservas) que compartan id con vuelo y luego podremos eliminar el vuelo.

 Respecto de asignar un mismo asiento a dos pasajeros diferentes, si tomamos unicamente las restricciones de la base de datos (admite enteros y no debe ser un campo vacio) nos presenta problemas ya que nos permitiría asignar el mismo asiento a muchas personas en el mismo vuelo o asientos inexistentes. Sin embargo, en nuestro caso, esas limitaciones las aplicaremos mendiante código en java, realizando búsquedas para verificar la existencia del asiento y su disponiblidad.

 ## Transacciones

 Una opción dentro de las configuraciones que tiene mysql es la del AUTOCOMMIT esta nos permite configurar si queremos que nuestros cambios sobre la tabla se guarden automaticamente o si, por el contrario necesitar una confirmación posterior pudiendo deshacer esos cambios. 

Con la siguiente sentencia podemos ver el estado en que se encuentra el autocommit. La opción `` ON`` es la que viene por defecto.
```sql
show variables like 'autocommit';
```

 Con la opción de autocommit=0 estaremos desactivando la opción de autocommit, pudiendo luego descartar los cambios con ``rollback`` y dejando la tabla de la misma manera en que estaba previa a los cambios. 
```sql
set autocommit=0;

```
Esta sentencia aunque no obligatoria, puede ser útil para indicar los cambios a abarcar en caso de confirmar o volver atrás. 
```sql 
start transaction;
```
Por ejemplo, a nuestra pasajera Elsa podemos cambiarle el nombre y con un ``rollback `` volver atrás con esos cambios.  
```sql
UPDATE pasajeros SET nombre="Helga Pato" WHERE id_pasajero = 6;

```
```sql
rollback;

```
La opción commit confirma nuestros cambios. 
```sql
commit;

```
Por su parte, autocommit=1 vuelve a activar la autoconfirmación.
```sql
set autocommit=1;

```




