CREATE TABLE `Reservas`.`Vuelos` (
  `id_vuelo` VARCHAR(6) NOT NULL,
  `origen` VARCHAR(11) NOT NULL,
  `destino` VARCHAR(11) NOT NULL,
  `fecha` DATE NOT NULL,
  `capacidad` INT(3) NOT NULL,
  PRIMARY KEY (`id_vuelo`)
);