CREATE TABLE `Vuelos_Pasajeros` (
  `id_reserva` int NOT NULL AUTO_INCREMENT,
  `id_vuelo` varchar(6) NOT NULL,
  `id_pasajero` int NOT NULL,
  `num_asiento` int NOT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `Vuelos_Pasajeros_ibfk_1` FOREIGN KEY (`id_vuelo`) REFERENCES `Vuelos` (`id_vuelo`),
  CONSTRAINT `Vuelos_Pasajeros_ibfk_2` FOREIGN KEY (`id_pasajero`) REFERENCES `Pasajeros` (`id_pasajero`)
);