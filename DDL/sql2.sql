CREATE TABLE `Reservas`.`Pasajeros` (
  `id_pasajero` INT NOT NULL UNIQUE AUTO_INCREMENT,
  `pasaporte` VARCHAR(9) NOT NULL UNIQUE,
  `nombre` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id_pasajero`)
);