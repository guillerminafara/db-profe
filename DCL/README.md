
María Guillermina, Fara Santeyana
# Introducción 
En el siguiente trabajo nos desplegaremos sobre la administración de la base de datos, sobre la gestión de sus usuarios, permisos, privilegios, roles y cómo gestionarlos para sostener la seguridad de los datos en nuestra BD.
El código fuente correspondiente a este reto puede consultarse en:[LINK GITHUB](https://gitlab.com/guillerminafara/db-profe/-/tree/main/DCL?ref_type=heads)

## Información sobre la base 
### Listar usuarios
 Esta consulta nos lista los usuarios que hay creados para gestionar con el SGBD.
```sql 
SELECT user FROM mysql.user; 
```
nota aparte: me ha saltado este error: `` #1034 - Clave de archivo erronea para la tabla: 'global_priv'; intente repararlo ``. Según ChatGPT puede deberse a una interrupción o corrupción en los archivos de la base de datos. Y lo he solucionado con la siguiente sql 
```sql
REPAIR TABLE mysql.global_priv;
```
* El siguiente comando nos muestra información sobre la base de datos, el tipo de datos que manejan, si admiten contenido null, contenido por defecto por ejemplo.
```sql
SHOW COLUMS FROM mysql.user  
```

### Crear usuario
La siguiente sentencia la utilizaremos para crear un usuario. Le indicaremos entre comillas simples el nombre del usuario @ seguido del host también entre comillas. Finalmente indicaremos ``IDENTIFIED BY `` para ingresar la contraseña con la que registraremos el usuario.
```sql
CREATE USER 'esteban quito'@'%' IDENTIFIED BY '1234';

``` 
En este caso estamos restringiendo a que el acceso se lleve a partir de una red local, lo señalamos en el host.
```sql
CREATE USER 'esteban quito'@'192.168.4%' IDENTIFIED BY '1234';

``` 


## Sobre el usuario

### Conectar con usuario
Esta sentencia nos sirve para conectarnos con el usuario una vez lo hayamos creado. Debemos completar con los siguientes datos en nuestra terminal.
``--user`` o ``-u``: el usuario a conectar
``--pasword`` o ``-p``: para indicar que nos loggeamos con contraseña 
``--host`` o ``-h``: completamos con nuestro host 
``--port`` o ``-P``: indicamos el puerto donde tenemos la base.
ej:
```bash 
mysql --user 'esteban quito' --password -h 127.0.0.1 -P 33006
```
ej:
```bash 
mysql --user root --password -h 127.0.0.1 --port 33006

```
### Ver el Host
Para ver el Host que tiene cada usuario podemos ejecutar el siguiente comando:
```sql
SELECT Host, User FROM mysql.user 
```

### Modificar usario
**Cambiar contraseña:**
Usamos ALTER USER que nos sirve para modificar algunos aspectos del usuario como el nombre o la contraseña. En esta ocasión en conjunto con IDENTIFIED BY  nos sirve para especificar una contraseña nueva.
```sql
ALTER USER 'esteban quito'@'%' IDENTIFIED BY '1235'; 
```
**Cambiar nombre:**
 En estos casos el RENAMED TO nos permite cambiar el nombre, puede usarse siguiendo a ALTER USER(en el primer ejemplo) como por sí solo(segundo ejemplo). También nos sirve en caso de querer modificar el host. 
 ```sql
ALTER USER 'esteban quito'@'%' RENAMED TO 'esteban'@'%';

```
```sql
RENAME USER 'esteban '@'192.168.4%' TO 'esteban quieto'@'localhost';
```

### Eliminar usuarios

```sql
DROP USER 'esteban quito'@'%';
```

### Bloqueo/Desbloqueo
**Bloquear usuario:** Con esta sentencia bloqueamos a Iván, no podrá ingresar a la sesión. Sin embargo, esto no finaliza las sesiones ya iniciadas con la cuenta de Iván.
```sql
ALTER USER "ivan" ACCOUNT LOCK;
```
Para localizar los  id de las sesiones activas de Iván ejecutaremos la siguiente sentencia para localizar el id de las sesiones activas posteriormente poder cerrarlas con el último comando. 
```sql
select id from information_schema.processlist where user = "ivan";
```
```bash
kill 77;
```
**Desbloquear usuario:** De esta forma devolvemos al usuario la posibilidad de volver a iniciar sesión conservando roles y privilegios con los que contaba.
```sql
ALTER USER "ivan" ACCOUNT UNLOCK;
```


## Roles y Permisos
### Permisos de usuarios 
**Ver permisos:** Este comando nos sirve para mostrar los privilegios que tiene el usuario, en este caso root. También estando logueado en un usuario como en el segundo caso.
tambien sirve para mostrar los roles 
```sql
SHOW GRANTS FOR root@'%';
```
```sql
SHOW GRANTS;
```
**Asignar privilegios:** De esta manera podemos asignar privilegios a la sesión de root sobre una tabla dentro de una base de datos específica.

```sql
GRANT SELECT ON Chinook.Track TO root@'%';
```

```sql
GRANT SELECT ON Chinook.Track TO USER root@'%';

GRANT SELECT ON Chinook.Playlist TO 'esteban quito'@'192.68';

GRANT SELECT ON Chinook.Artist TO 'esteban quito'@'192.68;

```

**Quitar privilegios:** Es este caso vemos como con esta sentencia le quitamos todos los privilegios que tenia en la base de datos chinook a Esteban 
```sql
REVOKE * PRIVILEGES ON Chinook.* FROM 'esteban quito'@'%';
```
**Crear roles:** Create Role nos sirve para crear un rol. Este rol por si solo no contará con ningún permiso ni privilegio, deberemos asignárselos. Una vez que ya los tiene (o no) podremos otorgar permisos a un grupo de usuarios por medio de la asignación de roles por ejemplo un rol llamado usuario raso, el cual por defecto no cuenta con ningún privilegio.
```sql
CREATE ROLE 'usuario raso';
```
**Asignar un rol:** De esta forma podemos asignarle un rol por defecto a ivan para delimimitar sus accesos, acciones, permisos, privilegios, etc.
```sql
 SET DEFAULT ROLE "usuario raso" to 'ivan'@'%';

 SET ROLE "rol" to "root"@"%"
```
**Mostrar roles:** Esta Sentencia nos muestra el rol con el que cuenta la sesión actual.
```sql
SELECT CURRENT_ROL();
```
grant select on chinook.Artist  to 'usuario raso '
grant ' usuario raso ' to 'encarna vales'@'%'

**Quitar roles:** Sentencia que utilizamos para poner quitar todos los roles a la sesión actual.
```sql
SET ROLE NONE;
```REVOKE 'rol_ejemplo' FROM 'usuario_ejemplo'@'host';

agregar permisos a un rol
 GRANT INSERT INTO, UPDATE on "nombre de base".* / o tabla to rol

asignar rol a un usuario desde admin
grant rol(trabajador) to "usuaerio"@"%"

sss
 select user/rol from mysqluser




[mysqlid]
lower_case_table_name=1